From 5a8ed8ea95730f139e49e1342e9817d9d3c78482 Mon Sep 17 00:00:00 2001
From: Stefano Rivera <stefanor@debian.org>
Date: Mon, 12 Oct 2015 21:53:08 +0200
Subject: setup.py

Patch-Name: setup.py
---
 setup.py | 28 ++++++++++++++++++++++++++++
 1 file changed, 28 insertions(+)
 create mode 100644 setup.py

diff --git a/setup.py b/setup.py
new file mode 100644
index 0000000..c511aa9
--- /dev/null
+++ b/setup.py
@@ -0,0 +1,28 @@
+#!/usr/bin/python
+# Multiplexer for invoking multiple setup.py in subdir
+# Copyright (C) 2009 Stefano Zacchiroli <zack@debian.org>
+# License: GNU GPL version 3 or above
+
+# Created: Sat, 30 May 2009 14:47:04 +0200
+# Last-Modified: Sat, 30 May 2009 14:47:04 +0200
+
+import os, string, sys
+
+if not os.environ.has_key('SUBDIRS') or not os.environ['SUBDIRS']:
+    print >> sys.stderr, "Can't find subdirs, please set SUBDIRS envvar"
+    sys.exit(3)
+else:
+    subdirs = os.environ['SUBDIRS'].split()
+setup_cmd = "python setup.py %s" % string.join(sys.argv[1:])
+
+topdir = os.getcwd()
+for d in subdirs:
+    if not os.path.isdir(d):
+        print >> sys.stderr, "WARNING: can't find subdir %s" % d
+        continue
+    os.chdir(d)
+    retcode = os.system(setup_cmd)
+    if retcode:
+        print >> sys.stderr, "ERROR: setup.py in subdir %s failed" % d
+        sys.exit(retcode >> 8)
+    os.chdir(topdir)
